
DESTDIR=/
BINDIR=$(DESTDIR)/usr/bin
SYSCONFDIR=$(DESTDIR)/etc
OPTDIR=$(DESTDIR)/opt
DATADIR=$(DESTDIR)/usr/share

.PHONY: all run clean edit lint lint_extra

all: run

edit:
	gedit -s src/mukke.py src/mukke/__init__.py src/mukke/base.py src/mukke/operator.py src/mukke/events.py src/mukke/model.py src/mukke/mpris.py src/mukke/inotify.py src/mukke/view.py src/mukke/player.py 2> /dev/null &

run:
	./src/mukke.py -v
	#./src/mukke.py

run_phone:
	./src/mukke.py -v -a

clean:
	-rm -r src/mukke/__pycache__

lint:
	# Ignored Errors and Warnings:
	# E265 block comment should start with '# '
	# W391 blank line at end of file
	-pycodestyle --ignore=E265,W391 --max-line-length=120 src/*

lint_extra:
	-pyflakes src/*
	#-pylint src/*
	#-flake8 src/*

upload:
	#rsync -a ./ manjaro@peepee.lan:mukke/
	rsync -a ./ manjaro@10.57.1.20:mukke/

install:
	mkdir -p $(BINDIR) $(SYSCONFDIR)/mukke $(OPTDIR)/mukke $(DATADIR)/applications
	cp -r src/* $(OPTDIR)/mukke/
	cp -r config/* $(SYSCONFDIR)/mukke/
	install -Dm755 mukke.sh $(BINDIR)/mukke
	cp mukke.desktop $(DATADIR)/applications/
	cp mukke-adaptive.desktop $(DATADIR)/applications/

uninstall:
	-rm $(SYSCONFDIR)/mukke
	-rm $(OPTDIR)/mukke
	-rm $(BINDIR)/mukke
	-rm $(DATADIR)/applications/mukke.desktop

