
# Mukke

Mukke is a no frills music player.

There is no database, just add folders as tabs/playlists.
Folders are monitored for added/removed files using inotify.

Status: in daily use, work in progess

![Screenshot](demo/mukke.png)


Mukke uses:

- [PyGObject](https://pygobject.readthedocs.io/en/latest/)
- [GStreamer](https://gstreamer.freedesktop.org/)


