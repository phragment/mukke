# This file is part of Mukke
# Copyright 2020-2022 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import resource
import subprocess

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gio, GLib


"""
Events encapsulate what the application does.
"""

class Event:

    """
    asdf
    """

    def process(self, operator):
        raise NotImplementedError("no process function")


class Quit(Event):

    def process(self, operator):
        operator.log.debug("shutting down")

        # store window size
        operator.view.save_window_state()

        operator.view.stop()  # TODO wait?

        operator.model.save_directories()

        operator.stop()


class LoadDirectories(Event):

    def __init__(self, fp):
        self.fp = fp

    def process(self, operator):
        operator.log.debug("load directories")
        operator.model.load_directories(self.fp)


class AddDirectory(Event):

    def __init__(self, dp):
        self.dp = dp

    def process(self, operator):
        operator.log.debug("add directory")
        operator.model.add_directory(self.dp)


class RemoveDirectory(Event):

    def __init__(self, dp):
        self.dp = dp

    def process(self, operator):
        operator.log.debug("remove directory")
        operator.model.remove_directory(self.dp)


class Play(Event):

    def __init__(self, ident, fp):
        self.ident = ident
        self.fp = fp

    def process(self, operator):
        operator.log.debug("playback start")
        operator.model.player.play(self.ident, self.fp)


# TODO unify?
class Play2(Event):

    def process(self, operator):
        operator.log.debug("playback start 2")
        ident, fp = operator.view.get_cur()
        operator.model.player.play(ident, fp)


class Stop(Event):

    def process(self, operator):
        operator.log.debug("playback stop")
        operator.model.player.stop()


class Pause(Event):

    def process(self, operator):
        operator.log.debug("playback pause")
        operator.model.player.pause()


class Prev(Event):

    def process(self, operator):
        operator.log.debug("playback prev")
        #ident, fp = operator.view.get_cur()
        #fp = operator.view.get_prev(ident)
        #operator.model.player.play(ident, fp)
        operator.model.player.prev()


class Next(Event):

    def process(self, operator):
        operator.log.debug("playback next")
        #ident, fp = operator.view.get_cur()
        #fp = operator.view.get_next(ident)
        #operator.model.player.play(ident, fp)
        operator.model.player.next()


class SetVolume(Event):

    def __init__(self, volume):
        self.volume = volume

    def process(self, operator):
        operator.log.debug("set volume")
        operator.model.player.set_volume(self.volume)


class OpenExternal(Event):

    def __init__(self, uri):
        self.uri = uri
        self.cmd_open = "/usr/bin/xdg-open"

    def process(self, operator):
        operator.log.debug("opening externally: %s", self.uri)
        try:
            ret = subprocess.call(
                [self.cmd_open, self.uri],
                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
            )
        except FileNotFoundError:
            operator.log.error("command not found: %s", self.cmd_open)


class SetModeNormal(Event):

    def process(self, operator):
        operator.log.debug("set playback mode to normal")
        operator.model.player.set_mode_normal()


class SetModeRandom(Event):

    def process(self, operator):
        operator.log.debug("set playback mode to random")
        operator.model.player.set_mode_random()


class ElevatePriority(Event):

    def __init__(self):
        self.prio = 9

    def process(self, operator):
        self.log = operator.log
        operator.log.debug("trying to elevate scheduling priority")

        if not self.direct(self.prio):
            operator.log.debug("failed, trying via rtkit")
            if not self.rtkit(self.prio):
                operator.log.warning("failed elevating scheduling priority")
                return

        operator.log.debug("successfully elevated prio")

    def direct(self, prio, tid=0):
        #resource.setrlimit(resource.RLIMIT_RTTIME, (200000, 200000))

        try:
            os.nice(-10)
        except PermissionError:
            return False

        policy = os.SCHED_RR
        sched_prio = os.sched_param(prio)

        try:
            os.sched_setscheduler(tid, policy, sched_prio)
        except PermissionError:
            return False

        return True

    def rtkit(self, prio, tid=0):
        con = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)

        bus_name = "org.freedesktop.RealtimeKit1"
        obj = "/org/freedesktop/RealtimeKit1"

        interface = "org.freedesktop.DBus.Properties"
        method = "Get"
        propname = GLib.Variant.new_string("RTTimeUSecMax")
        iface = GLib.Variant.new_string("org.freedesktop.RealtimeKit1")
        param = GLib.Variant.new_tuple(iface, propname)
        ret = con.call_sync(bus_name, obj, interface, method, param, None, Gio.DBusCallFlags.NONE, -1, None)
        rttime = ret[0]

        self.log.debug("limiting rttime to %s", rttime)
        resource.setrlimit(resource.RLIMIT_RTTIME, (rttime, rttime))

        interface = "org.freedesktop.RealtimeKit1"
        method = "MakeThreadRealtime"
        tid_ = GLib.Variant.new_uint64(tid)
        prio_ = GLib.Variant.new_uint32(prio)
        param = GLib.Variant.new_tuple(tid_, prio_)
        try:
            con.call_sync(bus_name, obj, interface, method, param, None, Gio.DBusCallFlags.NONE, -1, None)
        except GLib.GError as e:
            self.log.debug("elevating prio via rtkit failed", exc_info=e)
            return False

        return True













