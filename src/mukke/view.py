# This file is part of Mukke
# Copyright 2020-2022 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import datetime
import json
import os
import random
import urllib


import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib, Gdk, GObject, Gio, Pango

import mukke.events


# decorator for enqueuing functions into gtk main thread
def gtk(func):
    def wrapper(*args, **kwargs):
        #print("view wrapper, called by", threading.current_thread().name)
        GLib.idle_add(func, *args, **kwargs)
    return wrapper


"""
The View manages the User Interface.
"""
class View:

    def __init__(self, log, settings, operator):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.operator = operator

        self.inhibit_cookie = 0

        random.seed()

        # TODO how to correctly access parent class from all widgets?
        # how to access model correctly?
        global VIEW
        VIEW = self

    def start(self):
        self.log.debug("starting")
        self.app = Application(self)
        self.app.run()

    @gtk
    def stop(self):
        self.log.debug("stopping")

        #self.app.win.win.close()
        self.app.win.win.destroy()

    @gtk
    def new_tab(self, ident):
        self.app.win.new_tab(ident)

    @gtk
    def close_tab(self, ident):
        self.app.win.close_tab(ident)

    @gtk
    def add_file(self, ident, f, data):
        #print(ident, data)

        # add to liststore
        tab = self.app.win.get_tab(ident)
        if not tab:
            return

        tab.add_file(f, data)

    @gtk
    def remove_file(self, ident, f):
        tab = self.app.win.get_tab(ident)
        if not tab:
            return

        tab.remove_file(f)

    def get_prev(self, ident, cfp):
        tab = self.app.win.get_tab(ident)
        if not tab:
            return None
        prev = tab.get_prev(cfp)
        return prev

    def get_next(self, ident, cfp):
        tab = self.app.win.get_tab(ident)
        if not tab:
            return None
        next = tab.get_next(cfp)
        return next

    def get_random(self, ident):
        tab = self.app.win.get_tab(ident)
        fp = tab.get_random()
        return fp

    def get_cur(self):
        tab = self.app.win.cur_tab()
        fp = tab.get_selected()
        #print("get_cur", tab.ident, fp)
        return tab.ident, fp

    @gtk
    def update_seekbar(self, duration, position):
        self.app.win.status.update_seekbar(duration, position)

    @gtk
    def update_volume(self, volume):
        vol = self.app.win.toolbar.vol
        # mask value-changed signal
        vol.handler_block_by_func(self.app.win.toolbar.on_volume_changed)
        vol.set_value(volume)
        vol.handler_unblock_by_func(self.app.win.toolbar.on_volume_changed)

    @gtk
    def update_info(self, info):
        data = {
            "state": "stopped",
            "dir": "",
            "file": ""
        }
        data.update(info)

        if self.settings["adaptive"]:
            text = data["state"]
            if data["file"]:
                text += ' {}'.format(data["file"])
            if data["dir"]:
                text += ' ({})'.format(data["dir"])

            text = GLib.markup_escape_text(text)

            l = 25
            if len(text) > l:
                text = text[:l] + "..."
            self.app.win.set_subtitle(text)
        else:
            text = '<span size="medium">'
            text += '{}'.format(GLib.markup_escape_text(data["state"]))
            if data["file"]:
                text += ' <b>{}</b>'.format(GLib.markup_escape_text(data["file"]))
            if data["dir"]:
                dn = os.path.basename(data["dir"])
                text += ' ({})'.format(GLib.markup_escape_text(dn))
            text += '</span>'

            self.app.win.status.set_info(text)

        # select current track and scroll to it
        d = data["dir"]
        if d:
            tab = self.app.win.tabs[d]
            if tab:
                if data["file"]:
                    tab.select(data["file"])

    @gtk
    def stop_sleeping(self, sleep):
        if sleep:
            self.inhibit_cookie = self.app.app.inhibit(
                None,
                Gtk.ApplicationInhibitFlags.SUSPEND,
                "playing music"
            )
        else:
            if self.inhibit_cookie != 0:
                self.app.app.uninhibit(self.inhibit_cookie)

    @gtk
    def save_window_state(self):
      size = self.app.win.win.get_default_size()
      self.log.debug(size)
      state = {"window": {"width": size.width, "height": size.height}}
      with open(self.settings["state"], "w") as f:
          json.dump(state, f, sort_keys=True, indent=4)



class Application:

    def __init__(self, view):
        self.view = view
        self.app = Gtk.Application.new(None, 0)

        appid = "de.elektronenpumpe.mukke.pid{}".format(os.getpid())
        self.app.set_property("application-id", appid)

        self.app.connect("activate", self.activate)

    def run(self):
        self.app.run()

    def activate(self, app):
        self.win = MainWindow(app, self.view)


class MainWindow:

    def __init__(self, app, view):
        self.view = view
        self.tabs = {}

        self.win = Gtk.ApplicationWindow.new(app)

        self.win.set_title("Mukke")
        self.win.set_icon_name("audio-headphones")

        self.win.connect("close-request", self.on_window_close_request)

        if self.view.settings["adaptive"]:
            self.win.maximize()  # TODO only if small screen?
            self.setup_headerbar()
        else:
            width = 900
            height = 1000

            try:
                with open(self.view.settings["state"], "r") as f:
                    state = json.load(f)
                    width = state["window"]["width"]
                    height = state["window"]["height"]
                    self.view.log.debug("restoring window size %s", state)
            except Exception:
                self.view.log.info("could not load window size")

            #self.win.set_size_request(width, height)
            self.win.set_default_size(width, height)
            #self.win.resize(width, height)  # gtk3 only?
            #self.win.set_resizable(True)

        self.vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.win.set_child(self.vbox)

        # TODO this has to look good
        self.status = StatusBar(self.view)
        self.vbox.append(self.status.get_widget())

        #self.notebook = Gtk.Notebook.new()
        #self.notebook.set_tab_pos(Gtk.PositionType.LEFT)
        #self.notebook.set_scrollable(True)
        #self.vbox.append(self.notebook)
        #if self.view.settings["adaptive"]:
        #    self.notebook.set_show_tabs(False)

        self.notebook = ModdedNotebook()
        self.vbox.append(self.notebook.get_widget())


        self.toolbar = Toolbar(self.view)
        self.vbox.append(self.toolbar.get_widget())

        self.searchbar = Searchbar(self.view)
        self.vbox.append(self.searchbar.get_widget())


        ## shortcuts
        self.sc = Gtk.ShortcutController.new()
        self.sc.set_scope(Gtk.ShortcutScope.GLOBAL)
        self.sc.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
        self.win.add_controller(self.sc)

        #trigger = Gtk.KeyvalTrigger.new(Gdk.KEY_Tab, Gdk.ModifierType.CONTROL_MASK)
        #action = Gtk.CallbackAction.new(self.next_tab_action)
        #shortcut = Gtk.Shortcut.new(trigger, action)
        #self.sc.add_shortcut(shortcut)

        #trigger = Gtk.KeyvalTrigger.new(Gdk.KEY_Tab, Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK)
        #action = Gtk.CallbackAction.new(self.prev_tab_action)
        #shortcut = Gtk.Shortcut.new(trigger, action)
        #self.sc.add_shortcut(shortcut)

        self.notebook.setup_shortcuts(self.sc)

        # search
        trigger = Gtk.KeyvalTrigger.new(Gdk.KEY_f, Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK)
        action = Gtk.CallbackAction.new(self.show_searchbar_action)
        shortcut = Gtk.Shortcut.new(trigger, action)
        self.sc.add_shortcut(shortcut)

        self.win.show()
        self.searchbar.hide()

        if self.view.settings["adaptive"]:
            self.notebook.set_show_tabs(False)

        self.view.log.debug("init done")

    def on_window_close_request(self, window):
        self.view.operator.enqueue(mukke.events.Quit())
        # return event handled, otherwise window shuts down immediately
        return True

    def new_tab(self, ident):
        tab = Tab(self.view, ident)
        self.tabs[ident] = tab
        #box = tab.get_widget()
        #label = tab.get_label()
        #self.notebook.append_page(box, label)
        self.notebook.add_tab(tab)

    def close_tab(self, ident):
        self.view.log.debug("closing tab %s", ident)
        tab = self.tabs[ident]
        #self.notebook.detach_tab(tab.get_widget())
        self.notebook.remove_tab(tab)
        self.tabs[ident] = None

    def get_tab(self, ident):
        return self.tabs.get(ident, None)

    def cur_tab(self):
        return self.notebook.cur_tab()

    def show_searchbar_action(self, widget, args):
        self.searchbar.show()

    def setup_headerbar(self):
        self.headerbar = Gtk.HeaderBar()
        self.win.set_titlebar(self.headerbar)

        #self.headerbar.set_title(None)  # display window title
        #self.headerbar.set_subtitle("test")

        self.titlebox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.title = Gtk.Label.new("Mukke")
        self.title.set_vexpand(True)
        self.title.add_css_class("title")
        self.titlebox.append(self.title)
        self.subtitle = Gtk.Label.new("")
        self.subtitle.set_vexpand(True)
        self.subtitle.add_css_class("subtitle")
        self.titlebox.append(self.subtitle)
        self.headerbar.set_title_widget(self.titlebox)

        self.headerbar.set_show_title_buttons(False)


        self.button_overflow = Gtk.Button.new_from_icon_name("view-more")
        self.button_overflow.connect("clicked", self.on_hbb_overflow_clicked)
        self.headerbar.pack_end(self.button_overflow)

        button_tabs = Gtk.Button.new_from_icon_name("view-list-bullet")
        button_tabs.connect("clicked", self.on_hbb_tabs_clicked)
        self.headerbar.pack_end(button_tabs)

        self.setup_overflow_menu()

    def set_subtitle(self, markup):
        self.subtitle.set_markup(markup)

    def on_hbb_tabs_clicked(self, button):
        if self.notebook.get_show_tabs():
            self.notebook.set_show_tabs(False)
        else:
            self.notebook.set_show_tabs(True)

    def on_hbb_overflow_clicked(self, button):
        if not self.popover.get_parent():
            self.popover.set_parent(button)
        self.popover.popup()

    def setup_overflow_menu(self):
        self.popover = Gtk.Popover()
        self.popover.set_position(Gtk.PositionType.BOTTOM)

        self.pobox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.popover.set_child(self.pobox)

        button = self.create_button("Exit", "window-close", self.on_po_exit_clicked)
        self.pobox.append(button)


    def create_button(self, label_text, icon_name, cb):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        icon = Gtk.Image.new_from_icon_name(icon_name)
        box.append(icon)

        label = Gtk.Label.new(label_text)
        label.set_xalign(0)
        box.append(label)

        button = Gtk.Button.new()
        #button = Gtk.ModelButton.new()
        button.set_child(box)

        button.connect("clicked", cb)

        return button


    def on_po_exit_clicked(self, button):
        self.win.close()


class ModdedNotebook:

    def __init__(self):
        self.tabs = {}
        self.visible = True

        self.box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)

        self.sw = Gtk.ScrolledWindow.new()
        self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.box.append(self.sw)

        self.bookmarks = Gtk.ListBox.new()
        self.sw.set_child(self.bookmarks)

        self.notebook = Gtk.Notebook.new()
        self.notebook.set_show_tabs(False)

        self.notebook.set_hexpand(True)
        self.notebook.set_vexpand(True)
        self.box.append(self.notebook)

        self.bookmarks.connect("row-selected", self.on_listbox_row_selected)
        self.notebook.connect("switch-page", self.on_switch_page)

        self.setup_style()

    def get_widget(self):
        return self.box

    def add_tab(self, tab):
        self.tabs[tab.ident] = tab

        #tab.set_parent(self)

        label = tab.get_label()
        widget = tab.get_widget()

        self.notebook.append_page(widget, None)
        self.bookmarks.append(label)

    def remove_tab(self, tab):
        #self.notebook.remove_page(page_num)
        self.notebook.detach_tab(tab.get_widget())
        label = tab.get_label()
        listboxrow = label.get_parent()
        self.bookmarks.remove(listboxrow)

    def on_switch_page(self, notebook, page, n):
        # update selection
        row = self.bookmarks.get_row_at_index(n)
        self.bookmarks.select_row(row)

    def on_listbox_row_selected(self, listbox, row):
        if not row:
            return

        label = row.get_child()

        n = -1
        for k, v in self.tabs.items():
            if v.get_label() == label:
                n = self.notebook.page_num(v.get_widget())
                break

        if not n == -1:
            self.notebook.set_current_page(n)

    def cur_tab(self):
        # we need to reference back to our identifier
        pos = self.notebook.get_current_page()

        ## hacky
        # get current widget
        widget = self.notebook.get_nth_page(pos)
        # iterate over self.tabs, compare tab.box pointer
        for ident, tab in self.tabs.items():
            if tab.get_widget() == widget:
                return tab

        return None

    def next_tab(self):
        # does not cycle
        #self.notebook.next_page()

        pos = self.notebook.get_current_page()
        num = self.notebook.get_n_pages()
        if num == 0:
            return
        pos = (pos + 1) % num
        self.notebook.set_current_page(pos)

    def prev_tab(self):
        # does not cycle
        #self.notebook.prev_page()

        pos = self.notebook.get_current_page()
        num = self.notebook.get_n_pages()
        if num == 0:
            return
        pos = (pos - 1) % num
        self.notebook.set_current_page(pos)

    def next_tab_action(self, widget, args):
        self.next_tab()
        return True

    def prev_tab_action(self, widget, args):
        self.prev_tab()
        return True

    def setup_style(self):
        style = """
        #tab-label {
          min-height: 24px;
          margin: 4px;
        }

        #tab-label > * {
          margin-left: 8px;
        }
        """
        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)

        display = Gdk.Display.get_default()
        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        Gtk.StyleContext.add_provider_for_display(display, provider, priority)

    def setup_shortcuts(self, sc):
        # next tab
        trigger = Gtk.KeyvalTrigger.new(Gdk.KEY_Tab, Gdk.ModifierType.CONTROL_MASK)
        action = Gtk.CallbackAction.new(self.next_tab_action)
        shortcut = Gtk.Shortcut.new(trigger, action)
        sc.add_shortcut(shortcut)

        # previous tab
        trigger = Gtk.KeyvalTrigger.new(Gdk.KEY_Tab, Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK)
        action = Gtk.CallbackAction.new(self.prev_tab_action)
        shortcut = Gtk.Shortcut.new(trigger, action)
        sc.add_shortcut(shortcut)

    def get_show_tabs(self):
        return self.visible

    def set_show_tabs(self, visible):
        if visible:
            self.bookmarks.show()
        else:
            self.bookmarks.hide()
        self.visible = visible


class Entry(GObject.Object):

    def __init__(self, ident, columns):
        super().__init__()
        self.ident = ident  # intended for mapping to backend model
        self.columns = columns

    @staticmethod
    def create(col):
        widget = Gtk.Label()
        widget.props.xalign = 0  # align left

        widget.props.wrap_mode = Pango.WrapMode.WORD_CHAR
        widget.props.wrap = True
        widget.props.width_chars = 15  # treated as minimum
        #widget.props.max_width_chars = 20

        return widget

    def bind(self, widget, col):
        try:
            text = self.columns[col]
        except IndexError:
            text = ""
        widget.set_text(text)

    @staticmethod
    def sort(a, b, col):
        try:
            a = a.columns[col]
        except IndexError:
            a = ""

        try:
            b = b.columns[col]
        except IndexError:
            b = ""

        if a.lower() < b.lower():
            return Gtk.Ordering.SMALLER
        if a.lower() > b.lower():
            return Gtk.Ordering.LARGER
        return Gtk.Ordering.EQUAL

    def __repr__(self):
        return str(self.columns)


class ColumnView:

    def __init__(self, view, ident):
        self.view = view
        self.ident = ident

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.box.props.hexpand = True
        self.box.props.vexpand = True

        self.sw = Gtk.ScrolledWindow()
        self.sw.props.hscrollbar_policy = False
        self.box.append(self.sw)

        self.columnview = Gtk.ColumnView()
        self.columnview.props.hexpand = True
        self.columnview.props.vexpand = True
        self.sw.set_child(self.columnview)

        self.model = Gio.ListStore()

        self.filter_substr = ""
        self.filter = Gtk.CustomFilter.new(self.match)
        self.filtermodel = Gtk.FilterListModel.new(self.model, self.filter)

        cv_sorter = self.columnview.get_sorter()
        self.sortmodel = Gtk.SortListModel.new(self.filtermodel, cv_sorter)

        self.selmodel = Gtk.SingleSelection.new(self.sortmodel)

        self.columnview.set_model(self.selmodel)

        self.fac1 = Gtk.SignalListItemFactory()
        self.fac1.connect("bind", self.on_bind, 0)
        self.fac1.connect("setup", self.on_setup, 0)

        self.col1 = Gtk.ColumnViewColumn.new("Filename", self.fac1)
        self.columnview.append_column(self.col1)
        self.col1.props.expand = True

        self.fac2 = Gtk.SignalListItemFactory()
        self.fac2.connect("bind", self.on_bind, 1)
        self.fac2.connect("setup", self.on_setup, 1)

        self.col2 = Gtk.ColumnViewColumn.new("Modified", self.fac2)
        self.columnview.append_column(self.col2)

        sorter1 = Gtk.CustomSorter.new(Entry.sort, 0)
        self.col1.set_sorter(sorter1)

        sorter2 = Gtk.CustomSorter.new(Entry.sort, 1)
        self.col2.set_sorter(sorter2)

        # set default
        self.columnview.sort_by_column(self.col2, Gtk.SortType.DESCENDING)

        self.columnview.connect("activate", self.on_row_activated)

        self.entry = Gtk.Entry()
        self.box.append(self.entry)
        self.entry.connect("activate", self.on_search)

    def get_widget(self):
        return self.box

    def on_search(self, entry):
        text = entry.get_text()
        self.set_filter(text)

    def match(self, item):
        for text in item.columns:
            if self.filter_substr.lower() in text.lower():
                return True
        return False

    def set_filter(self, substr):
        self.filter_substr = substr
        self.filter.changed(Gtk.FilterChange.DIFFERENT)

    def on_setup(self, factory, listitem, col):
        widget = Entry.create(col)
        listitem.set_child(widget)

        drag_source = Gtk.DragSource()
        da = Gdk.DragAction.ASK
        da |= Gdk.DragAction.COPY
        da |= Gdk.DragAction.MOVE
        drag_source.set_actions(da)
        widget.add_controller(drag_source)
        widget._drag_source = drag_source

    def on_bind(self, factory, listitem, col):
        pos = listitem.get_position()  # position in model
        entry = self.sortmodel.get_item(pos)  # NOTE this is correct
        widget = listitem.get_child()
        entry.bind(widget, col)

        # TODO ident should be independent of dir path
        fp = os.path.join(self.ident, entry.ident)

        uri = "file://" + urllib.parse.quote(fp)
        mime = "text/uri-list"
        data = GLib.Bytes.new(uri.encode())
        prov = Gdk.ContentProvider.new_for_bytes(mime, data)
        widget._drag_source.set_content(prov)

    def append(self, ident, contents):
        entry = Entry(ident, contents)
        self.model.append(entry)

    def remove(self, ident):
        # TODO use _find_entry
        for i in range(self.model.get_n_items()):
            entry = self.model.get_item(i)
            if entry.ident == ident:
                self.model.remove(i)
                return

    def on_row_activated(self, columnview, position):
        entry = self.sortmodel.get_item(position)

        # TODO ident should be independent of dir path
        fp = os.path.join(self.ident, entry.ident)

        self.view.operator.enqueue(mukke.events.Play(self.ident, fp))

    def _find_entry(self, ident):
        for i in range(self.sortmodel.get_n_items()):
            entry = self.sortmodel.get_item(i)
            if entry.ident == ident:
                return i, entry
        return -1, None

    def get_prev(self, ident):
        pos, entry = self._find_entry(ident)
        if not entry:
            print("TODO")
            return

        pos -= 1
        if pos < 0:
            pos = self.sortmodel.get_n_items() - 1

        entry = self.sortmodel.get_item(pos)
        fp = os.path.join(self.ident, entry.ident)

        return fp

    def get_next(self, ident):
        pos, entry = self._find_entry(ident)
        if not entry:
            print("TODO")
            return

        pos += 1
        if pos >= self.sortmodel.get_n_items():
            pos = 0

        entry = self.sortmodel.get_item(pos)
        fp = os.path.join(self.ident, entry.ident)

        return fp

    def select(self, ident):
        pos, entry = self._find_entry(ident)
        if not entry:
            return

        #self.selmodel.set_selected(pos)
        self.columnview.scroll_to(pos, None, Gtk.ListScrollFlags.SELECT, None)

    def get_selected(self):
        pos = self.selmodel.get_selected()
        entry = self.sortmodel.get_item(pos)
        fp = os.path.join(self.ident, entry.ident)
        return fp

    def get_random(self):
        num = self.sortmodel.get_n_items()
        pos = random.randint(0, num)
        entry = self.sortmodel.get_item(pos)
        fp = os.path.join(self.ident, entry.ident)
        return fp


class Tab:

    def __init__(self, view, ident):
        self.view = view
        self.ident = ident

        self.label_box = None
        self.box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 1)

        self.columnview = ColumnView(self.view, self.ident)
        self.box.append(self.columnview.get_widget())

        self.style_list()

    def get_widget(self):
        return self.box

    def get_label(self):
        if not self.label_box:
            self.label_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
            self.label_box.set_name("tab-label")

            #text = self.ident
            text = self.ident.split("/")[-1]

            self.label = Gtk.Label.new(text)
            self.label.set_xalign(0)
            self.label.set_hexpand(True)
            self.label_box.append(self.label)

            self.button_close = Gtk.Button.new_from_icon_name("window-close")
            self.button_close.connect("clicked", self.on_button_clicked)
            self.button_close.set_name("tab-button-close")

            self.label_box.append(self.button_close)

            self.style_closebuttons()

        return self.label_box

    def style_closebuttons(self):
        style = """
        #tab-button-close {
          min-height: 0px;
          min-width: 0px;

          padding-top: 0px;
          padding-bottom: 0px;

          background-image: none;
          border-style: none;
        }
        """

        context = self.button_close.get_style_context()

        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)

        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION

        Gtk.StyleContext.add_provider(context, provider, priority)

    def style_list(self):
        style = """
        treeview#list * {
          padding-top: 1px;
          padding-bottom: 1px;
        }
        """

        context = self.columnview.columnview.get_style_context()
        #context = self.box.get_style_context()

        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)

        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION

        # FIXME
        #Gtk.StyleContext.add_provider(context, provider, priority)

    def on_button_clicked(self, button):
        self.view.operator.enqueue(mukke.events.RemoveDirectory(self.ident))

    def add_file(self, f, data):
        self.columnview.append(f, [data["filename"], data["modified"]])

    def remove_file(self, f):
        self.columnview.remove(f)

    def select(self, fp):
        self.columnview.select(fp)

    def get_selected(self):
        # used by play button
        return self.columnview.get_selected()

    def get_prev(self, cfp):
        return self.columnview.get_prev(cfp)

    def get_next(self, cfp):
        return self.columnview.get_next(cfp)

    def get_random(self):
        return self.columnview.get_random()



class StatusBar:

    def __init__(self, view):
        self.view = view

        # two lines
        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        # padding independent of css theming (and additional to!)
        padding = 2
        self.vbox.props.margin_start = padding
        self.vbox.props.margin_end = padding
        self.vbox.props.margin_top = padding
        self.vbox.props.margin_bottom = padding

        # first line
        self.box1 = Gtk.Box()
        self.box1.set_name("status")
        self.vbox.append(self.box1)

        self.label = Gtk.Label()
        self.label.set_hexpand(True)
        self.label.props.wrap = Gtk.WrapMode.WORD_CHAR

        self.box1.append(self.label)

        if self.view.settings["adaptive"]:
            self.box1.hide()

        # second line
        self.box2 = Gtk.Box()
        self.vbox.append(self.box2)

        self.time_cur = Gtk.Label.new("0:00")
        self.time_cur.props.margin_start = 5
        self.box2.append(self.time_cur)

        self.seekbar = Gtk.Scale.new(Gtk.Orientation.HORIZONTAL, None)
        self.seekbar.set_hexpand(True)
        self.box2.append(self.seekbar)

        self.seek = self.seekbar.get_adjustment()
        #self.seek.set_upper(100)  # active only after setting
        #self.seek.set_value(10)

        self.seekbar.connect("change-value", self.on_change_value)

        self.time_len = Gtk.Label.new("0:00")
        self.time_len.props.margin_end = 5
        self.box2.append(self.time_len)

        self.set_info()
        self.style()

    def get_widget(self):
        return self.vbox

    def set_info(self, markup=""):
        self.label.set_markup(markup)

    def update_seekbar(self, duration, position):
        # we get the timecodes in ns from gst
        self.seek.set_upper(duration)
        self.seek.set_value(position)
        #pos = time.localtime(position/1000/1000/1000)
        #dur = time.localtime(duration/1000/1000/1000)
        #self.time_cur.set_text(time.strftime("%H:%M:%S", pos))
        #self.time_len.set_text(time.strftime("%H:%M:%S", dur))
        pos = datetime.datetime.utcfromtimestamp(position/1e9)
        dur = datetime.datetime.utcfromtimestamp(duration/1e9)
        self.time_cur.set_text(pos.strftime("%H:%M:%S"))
        self.time_len.set_text(dur.strftime("%H:%M:%S"))

    def on_change_value(self, seekbar, scrolltype, value):
        # TODO show popover?
        #print(value)
        self.view.operator.model.player.seek(value)
        return True

    def style(self):

        style = """
        #status {

          padding-top: 10px;
          padding-bottom: 10px;

        }
        """

        context = self.box1.get_style_context()

        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)

        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION

        Gtk.StyleContext.add_provider(context, provider, priority)


class Toolbar:

    def __init__(self, view):
        self.view = view

        self.box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 1)
        self.box.set_name("toolbar")

        #self.open = Gtk.Button.new_from_icon_name("folder-open")
        self.add = Gtk.Button.new_from_icon_name("list-add")
        self.add.connect("clicked", self.on_add_folder)
        self.box.append(self.add)

        #self.open = Gtk.Button.new_from_icon_name("drive-harddisk")
        self.open = Gtk.Button.new_from_icon_name("folder-open")
        self.open.connect("clicked", self.on_open_folder)
        self.box.append(self.open)

        self.space1 = Gtk.Label.new("")
        self.space1.set_property("hexpand", True)
        self.space1.set_property("halign", Gtk.Align.FILL)
        self.box.append(self.space1)

        self.play = Gtk.Button.new_from_icon_name("media-playback-start")
        self.play.connect("clicked", self.on_play)
        self.box.append(self.play)

        self.pause = Gtk.Button.new_from_icon_name("media-playback-pause")
        self.pause.connect("clicked", self.on_pause)
        self.box.append(self.pause)

        self.stop = Gtk.Button.new_from_icon_name("media-playback-stop")
        self.stop.connect("clicked", self.on_stop)
        self.box.append(self.stop)

        self.prev = Gtk.Button.new_from_icon_name("media-skip-backward")
        self.prev.connect("clicked", self.on_prev)
        self.box.append(self.prev)

        self.next = Gtk.Button.new_from_icon_name("media-skip-forward")
        self.next.connect("clicked", self.on_next)
        self.box.append(self.next)

        self.shuffle = Gtk.ToggleButton.new()
        self.shuffle.set_child(Gtk.Image.new_from_icon_name("media-playlist-shuffle"))
        self.shuffle.connect("toggled", self.on_shuffle)
        self.box.append(self.shuffle)

        self.space2 = Gtk.Label.new("")
        self.space2.set_property("hexpand", True)
        self.space2.set_property("halign", Gtk.Align.FILL)
        self.box.append(self.space2)

        self.volscale = Gtk.VolumeButton.new()
        self.vol = self.volscale.get_adjustment()
        self.vol.set_upper(1.0)
        self.vol.set_step_increment(0.05)  # TODO
        self.vol.connect("value-changed", self.on_volume_changed)
        self.box.append(self.volscale)


    def get_widget(self):
        return self.box

    def on_play(self, button):
        self.view.log.debug("play")
        # get currently selected track from active tab
        cur = self.view.app.win.cur_tab()
        ident = cur.ident
        fp = cur.get_selected()
        self.view.operator.enqueue(mukke.events.Play(ident, fp))

    def on_pause(self, button):
        self.view.log.debug("pause")
        self.view.operator.enqueue(mukke.events.Pause())

    def on_stop(self, button):
        self.view.log.debug("stop")
        self.view.operator.enqueue(mukke.events.Stop())

    def on_prev(self, button):
        self.view.log.debug("prev")
        # get currently selected track from active tab
        #cur = self.view.app.win.cur_tab()
        #ident = cur.ident
        #fp = cur.get_prev()
        #self.view.operator.enqueue(mukke.events.Play(ident, fp))
        self.view.operator.enqueue(mukke.events.Prev())

    def on_next(self, button):
        self.view.log.debug("next")
        # get currently selected track from active tab
        #cur = self.view.app.win.cur_tab()
        #ident = cur.ident
        #fp = cur.get_next()
        #self.view.operator.enqueue(mukke.events.Play(ident, fp))
        self.view.operator.enqueue(mukke.events.Next())

    def on_shuffle(self, button):
        if button.get_active():
            #self.view.operator.model.player.set_mode_random()
            self.view.operator.enqueue(mukke.events.SetModeRandom())
        else:
            #self.view.operator.model.player.set_mode_normal()
            self.view.operator.enqueue(mukke.events.SetModeNormal())

    def on_add_folder(self, button):
        dialog = Gtk.FileChooserDialog()
        dialog.set_transient_for(self.view.app.win.win)

        dialog.set_property("title", "Choose Directory")
        dialog.set_property("action", Gtk.FileChooserAction.SELECT_FOLDER)

        # TODO use Gtk.ResponseType ?
        dialog.add_button("_Open", Gtk.ResponseType.ACCEPT)
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)

        dialog.connect("response", self.on_response)

        dialog.show()
        dialog.set_default_size(900, 600)

        # https://docs.gtk.org/gtk4/ctor.FileChooserDialog.new.html
        # https://docs.gtk.org/gtk4/method.Dialog.add_action_widget.html

    def on_response(self, dialog, response_id):
        if response_id == Gtk.ResponseType.CANCEL:
            dialog.destroy()

        if response_id == Gtk.ResponseType.ACCEPT:
            gf = dialog.get_current_folder()
            dialog.destroy()

            dp = gf.get_path()
            self.view.operator.enqueue(mukke.events.AddDirectory(dp))

    def on_volume_changed(self, adj):
        self.view.log.debug("updating volume")
        vol = adj.get_value()
        self.view.operator.enqueue(mukke.events.SetVolume(vol))

    def on_open_folder(self, button):
        cur = self.view.app.win.cur_tab()
        # TODO escape
        uri = "file://" + urllib.parse.quote(cur.ident)
        self.view.operator.enqueue(mukke.events.OpenExternal(uri))


class Searchbar:

    def __init__(self, view):
        self.view = view

        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        self.entry = Gtk.Entry()
        #self.hbox.pack_start(self.entry, True, True, 1)
        self.hbox.append(self.entry)
        self.entry.set_property("hexpand", True)
        self.entry.set_property("halign", Gtk.Align.FILL)

        self.entry.set_icon_from_icon_name(
            Gtk.EntryIconPosition.PRIMARY,
            "edit-find")

        self.entry.set_placeholder_text("search files")

        self.entry.connect("activate", self.on_entry_activated)

        self.button_close = Gtk.Button.new_from_icon_name("window-close")
        self.button_close.connect("clicked", self.on_button_close_clicked)
        self.hbox.append(self.button_close)



        self.sc = Gtk.ShortcutController()
        self.hbox.add_controller(self.sc)

        trigger = Gtk.KeyvalTrigger.new(Gdk.KEY_Escape, 0)
        action = Gtk.CallbackAction.new(self.hide_action)
        shortcut = Gtk.Shortcut.new(trigger, action)
        self.sc.add_shortcut(shortcut)



    def get_widget(self):
        return self.hbox

    def hide(self):
        self.hbox.hide()

    def hide_action(self, widget, args):
        self.hide()

    def show(self):
        self.hbox.show()
        self.entry.grab_focus()

    def on_button_close_clicked(self, button):
        self.hide()

    def on_entry_activated(self, entry):
        self.view.log.error("TODO implement search")













