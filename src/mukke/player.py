# This file is part of Mukke
# Copyright 2020-2022 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import enum
import os

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import GLib

gi.require_version("Gst", "1.0")
from gi.repository import Gst

import mukke.events


# decorator for enqueuing functions into gtk main thread
def gtk(func):
    def wrapper(*args, **kwargs):
        #print("view wrapper, called by", threading.current_thread().name)
        GLib.idle_add(func, *args, **kwargs)
    return wrapper


class PlaybackMode(enum.Enum):
    NORMAL = 1
    RANDOM = 2
    CURSOR = 4


class Player:

    def __init__(self, log, settings, view, cb_state):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.view = view
        self.cb_state = cb_state

        self.state = {
            "state": "stopped",
            "dir": "",
            "file": ""
        }

        self.mode = PlaybackMode.NORMAL

        Gst.init(None)

        self.pipeline = Gst.Pipeline.new("mypipe")

        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect("message", self.on_message)

        self.source = Gst.ElementFactory.make("filesrc", "source")
        self.buffer = Gst.ElementFactory.make("queue2", "buffer")
        self.decode = Gst.ElementFactory.make("decodebin", "decode")
        self.convert = Gst.ElementFactory.make("audioconvert", "convert")
        self.res = Gst.ElementFactory.make("audioresample", "res")
        if self.settings["replaygain"]:
            self.rg = Gst.ElementFactory.make("rgvolume", "rg")
        #self.sink = Gst.ElementFactory.make("autoaudiosink", "sink")
        self.sink = Gst.ElementFactory.make("pulsesink", "sink")
        self.sink.set_property("client-name", "mukke")

        self.pipeline.add(self.source)
        self.pipeline.add(self.buffer)
        self.pipeline.add(self.decode)
        self.pipeline.add(self.convert)
        self.pipeline.add(self.res)
        if self.settings["replaygain"]:
            self.pipeline.add(self.rg)
        self.pipeline.add(self.sink)

        self.source.link(self.buffer)
        self.buffer.link(self.decode)
        self.decode.connect("pad-added", self.on_pad_added)
        self.convert.link(self.res)
        if self.settings["replaygain"]:
            self.res.link(self.rg)
            self.rg.link(self.sink)
        else:
            self.res.link(self.sink)

        if self.settings["replaygain"]:
            self.rg.set_property("album-mode", False)
            self.rg.set_property("fallback-gain", self.settings["gain-fallback"])
            self.rg.set_property("pre-amp", self.settings["gain-preamp"])

        # buffer the current track as a whole
        self.buffer.set_property("use-buffering", True)
        self.buffer.set_property("max-size-buffers", 0)
        self.buffer.set_property("max-size-bytes", 0)
        self.buffer.set_property("max-size-time", 0)
        self.buffer.set_property("high-watermark", 1.0)
        self.buffer.set_property("low-watermark", 1.0)

        GLib.timeout_add(40, self.on_timer)

    def on_timer(self):
        # TODO use STATE_CHANGED to start/stop timer
        #print(".")
        GLib.timeout_add(40, self.on_timer)

        res, duration = self.pipeline.query_duration(Gst.Format.TIME)
        res, position = self.pipeline.query_position(Gst.Format.TIME)
        if not res:
            return
        #print("{} / {}".format(position/1000000, duration/1000000))

        self.view.update_seekbar(duration, position)


        #res, volume = self.sink.get_volume(GstAudio.StreamVolumeFormat.LINEAR)
        #print(volume)
        #self.view.update_volume()
        volume = self.sink.get_property("volume")
        #print(volume)
        self.view.update_volume(volume)

    @gtk
    def play(self, ident, fp):
        self.log.debug("play %s", fp)

        self.pipeline.set_state(Gst.State.NULL)
        self.source.set_property("location", fp)
        self.pipeline.set_state(Gst.State.PLAYING)

        self.state["dir"] = ident
        self.state["file"] = os.path.basename(fp)
        self.state["state"] = "playing"
        self.cb_state(self.state)

        self.view.stop_sleeping(True)

    @gtk
    def stop(self):
        self.log.debug("stop")
        # TODO set to READY to keep sink open?
        self.pipeline.set_state(Gst.State.NULL)

        #self.state["dir"] = ""
        self.state["file"] = ""
        self.state["state"] = "stopped"
        self.view.update_seekbar(0, 0)
        self.cb_state(self.state)

        self.view.stop_sleeping(False)

    @gtk
    def pause(self):
        self.log.debug("pause")
        ret, state, pending = self.pipeline.get_state(1)
        if state == Gst.State.PAUSED:
            self.pipeline.set_state(Gst.State.PLAYING)
            self.state["state"] = "playing"
        elif state == Gst.State.NULL:
            self.view.operator.enqueue(mukke.events.Play2())
        else:
            self.pipeline.set_state(Gst.State.PAUSED)
            self.state["state"] = "paused"
        self.cb_state(self.state)

        # TODO reenable sleep?
        self.view.stop_sleeping(False)

    def prev(self):
        self.log.debug("previous track")

        ident = self.state["dir"]
        cfp = self.state["file"]

        fp = self.view.get_prev(ident, cfp)
        if not fp:
            return
        self.play(ident, fp)

        self.cb_state(self.state)

    def next(self):
        self.log.debug("next track")

        # TODO check playback mode
        # random
        # playback follows cursor
        # ...

        ident = self.state["dir"]
        cfp = self.state["file"]

        # random
        if self.mode == PlaybackMode.RANDOM:
            fp = self.view.get_random(ident)

        # normal
        if self.mode == PlaybackMode.NORMAL:
            fp = self.view.get_next(ident, cfp)

        if not fp:
            return

        # playback follows cursor
        # check if cur sel == state
        #   if not selection is next track
        #   else get next track the normal way

        # only if playing?
        self.play(ident, fp)

        self.cb_state(self.state)


    def on_pad_added(self, gstelem, new_pad):
        self.decode.link(self.convert)

    def on_message(self, bus, message):
        # https://gstreamer.freedesktop.org/documentation/gstreamer/gstmessage.html#GstMessage
        #self.log.debug(message.type)

        if message.type == Gst.MessageType.ERROR:
            err = message.parse_error()
            self.log.error(err)
            # play next on error
            #self.view.operator.enqueue(mukke.events.Next())
            self.next()

        #if message.type == Gst.MessageType.STATE_CHANGED:
        #    print("state change")

        if message.type == Gst.MessageType.STREAM_START:
            self.log.debug("stream started")

        if message.type == Gst.MessageType.EOS:
            self.log.debug("end of stream")
            #self.view.operator.enqueue(mukke.events.Next())
            self.next()

        #if message.type == Gst.MessageType.DURATION_CHANGED:
            #print(message)
            #print(dir(message))

            #print("try to query duration")
            # time in nanosec
            #query = Gst.Query.new_duration(Gst.Format.TIME)
            #res, duration = self.source.query_duration(Gst.Format.TIME)
            #print("result?", duration)

        if message.type == Gst.MessageType.TAG:
            # TODO
            return

            taglist = message.parse_tag()
            self.log.debug(taglist)

            for i in range(taglist.n_tags()):
                name = taglist.nth_tag_name(i)
                self.log.debug(name)


    @gtk
    def set_volume(self, volume):
        #print(volume)
        self.sink.set_property("volume", volume)

    @gtk
    def seek(self, pos):
        rate = 1.0  # normal playback speed
        fmt = Gst.Format.TIME  # ns
        flags = Gst.SeekFlags.FLUSH
        start_type = Gst.SeekType.SET
        start = int(pos)
        stop_type = Gst.SeekType.NONE
        stop = -1

        self.pipeline.seek(rate, fmt, flags, start_type, start, stop_type, stop)


    def set_mode_random(self):
        self.mode = PlaybackMode.RANDOM

    def set_mode_normal(self):
        # TODO switch between normal and cursor according to config
        self.mode = PlaybackMode.NORMAL




