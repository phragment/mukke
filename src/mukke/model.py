# This file is part of Mukke
# Copyright 2020-2022 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import datetime
import json
import mimetypes
import os
import time

import mukke.player
import mukke.inotify
import mukke.mpris
import mukke.base


def check_audio(fn):
    # quick check if file could be audio
    typ, enc = mimetypes.guess_type(fn)
    if not typ:
        return False
    if typ.startswith("audio/"):
        return True
    else:
        return False


"""
The Model encapsulates everything the application knows.
"""

class Model:

    """
    asdf
    """

    def __init__(self, log, settings, view):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.view = view

        self.log.debug("before gst %s", time.time() - settings["ts_start"])
        self.player = mukke.player.Player(log, settings, self.view, self.on_state_changed)
        self.log.debug("after gst %s", time.time() - settings["ts_start"])

# so this is our database
# main key should be the directory the tab represents
# we need to cache filenames and metadata
# TODO move to own class, for hooking add/remove ?

        self.storage = {}

        self.monitor = mukke.inotify.Monitor(self)
        self.mpris = mukke.mpris.MPRIS(self)

        self.log.debug("init done")

# folder <> playlist
# inotify

    def start(self):
        self.mpris.start()

    def stop(self):
        self.mpris.stop()

    def on_state_changed(self, state):
        self.log.debug(state)
        self.mpris.state_changed(state)
        self.view.update_info(state)

    def load_directories(self, fp):
        self.log.debug("loading directories")
        self.storage_fp = fp

        self.log.debug("trying to read %s", fp)
        try:
            f = open(fp, "r")
            data = f.read()
        except FileNotFoundError:
            self.log.error("error reading storage")
            return

        try:
            storage = json.loads(data)
        except json.decoder.JSONDecodeError:
            self.log.error("error decoding json")
            return

        self.log.debug("parsed %s", storage)

        for ident, dp in storage.items():
            self.add_directory(dp)

    def save_directories(self):
        self.log.debug("saving directories")

        # generate dir if nonexistant
        mukke.base.mkdir(os.path.dirname(self.storage_fp))

        try:
            f = open(self.storage_fp, "w")
        except FileNotFoundError:
            self.log.error("error writing storage")
            return

        tmp = {}
        i = 1
        for k, v in self.storage.items():
            tmp[i] = k
            i += 1

        data = json.dumps(tmp, indent=4, sort_keys=False)
        f.write(data)
        f.close()

    def add_directory(self, dp):
        self.log.debug("adding directory %s", dp)

        dp = os.path.normpath(dp)

        if self.storage.get(dp, None):
            self.log.warning("already existant")
            return

        self.storage[dp] = {}

        self.view.new_tab(dp)

        if not os.path.exists(dp):
            return

        self.scan_directory(dp)

        self.monitor.add(dp)

        #print(self.storage)

    def remove_directory(self, dp):
        self.log.debug("removing directory %s", dp)
        self.view.close_tab(dp)
        self.monitor.remove(dp)
        del self.storage[dp]

    def scan_directory(self, dp):
        self.log.debug("scanning directory %s", dp)

        #for root, dirs, files in os.walk(dp):
        #    for f in files:
        #        add_file(dp, f)

        # TODO add only files (ideally playable by gstreamer)
        #for f in os.listdir(dp):
        #    self.add_file(dp, f)

        #for de in os.scandir(dp):
        #    if de.is_file():
        #        self.add_file(dp, de.name)

        # sort in view!
        for de in os.scandir(dp):
            if de.is_file():

                if not check_audio(de.name):
                    continue

                entry = {}
                entry["filename"] = de.name
                try:
                    tm = de.stat().st_mtime  # content modification
                    #tm = de.stat().st_ctime  # metadata change
                except FileNotFoundError:
                    continue
                dt = datetime.datetime.fromtimestamp(tm)
                entry["modified"] = dt.strftime("%Y-%m-%d %H:%M")

                # slow
                #print(mutagen.File(os.path.join(dp, de.name)))

                self.add_file(dp, de.name, entry)


    def add_file(self, dp, fn, info):
        #self.log.debug("adding file %s %s", dp, fn)
        self.storage[dp][fn] = info
        self.view.add_file(dp, fn, self.storage[dp][fn])

    def remove_file(self, fp):
        #self.log.debug("removing file %s", fp)
        dp = os.path.dirname(fp)
        fn = os.path.basename(fp)

        try:
            del self.storage[dp][fn]
        except KeyError:
            self.log.warning("file not in storage %s %s", dp, fn)

        self.view.remove_file(dp, fn)

    # TODO unify?
    def add_file2(self, fp):
        #self.log.debug("adding file %s", fp)
        dp = os.path.dirname(fp)
        fn = os.path.basename(fp)

        entry = {}
        entry["filename"] = fn

        try:
            mtime = os.stat(fp).st_mtime
        except FileNotFoundError:
            return
        dt = datetime.datetime.fromtimestamp(mtime)
        entry["modified"] = dt.strftime("%Y-%m-%d %H:%M")

        self.add_file(dp, fn, entry)





