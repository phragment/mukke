# This file is part of Mukke
# Copyright 2020-2022 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import time

import mukke.base
import mukke.operator
import mukke.events


class Mukke:

    """
    This is the starting point.

    - parse command line arguments
    - read config file
    - start operator
    - setup signal and exception handling
    - setup logging
    """

    def __init__(self):
        self.ts_start = time.time()
        self.scheme = {
            "storage": {"type": "datapath", "default": "storage.json"},  # rename to playlists/folders
            "state": {"type": "datapath", "default": "state.json"},
            "replaygain": {"type": "bool", "default": False, "desc": "enable replaygain"},
            "adaptive": {"type": "bool", "default": False, "desc": "mobile mode"},
            "gain-preamp": {"type": "int", "default": 0, "desc": ""},
            "gain-fallback": {"type": "int", "default": -10, "desc": ""}
        }

        app = mukke.base.Application("mukke", self.scheme, self.main, self.stop)
        app.run()

    def main(self, log, settings):
        settings["ts_start"] = self.ts_start
        log.debug("starting")

        self.operator = mukke.operator.Operator(log, settings)

        # TODO this crashes on pinephone manjaro (no rtkit?)
        # request high scheduler priority
        #event = mukke.events.ElevatePriority()
        #event.process(self.operator)

        # restore tabs
        self.operator.enqueue(mukke.events.LoadDirectories(settings["storage"]))

        self.operator.start()

    def stop(self):
        self.operator.enqueue(mukke.events.Quit())




