#!/usr/bin/env python3

import asyncio
import enum
import logging
import os
import threading

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gio, GLib

# python-dbus-next
import dbus_next

import mukke.events


class PlaybackStatus(enum.Enum):
    PLAYING = "Playing"
    STOPPED = "Stopped"
    PAUSED = "Paused"


class MPRIS:

    def __init__(self, model, log=None):
        self.operator = model.view.operator
        if log:
            self.log = log
        else:
            self.log = self.operator.log.getChild(type(self).__name__)

    def start(self):
        self.thread = threading.Thread(target=self.routine, name="MPRIS")
        self.thread.start()

    def routine(self):
        logging.getLogger("asyncio").setLevel(logging.WARNING)
        self.loop = asyncio.new_event_loop()
        self.loop.run_until_complete(self._start())

    async def _start(self):
        bus = await dbus_next.aio.MessageBus().connect()
        self.bus = bus

        self.iface = Interface(self.log)
        bus.export("/org/mpris/MediaPlayer2", self.iface)
        self.iface_player = InterfacePlayer(self)
        bus.export("/org/mpris/MediaPlayer2", self.iface_player)
        self.iface_playlists = InterfacePlaylists(self.log)
        bus.export("/org/mpris/MediaPlayer2", self.iface_playlists)
        self.iface_tracklist = InterfaceTrackList(self.log)
        bus.export("/org/mpris/MediaPlayer2", self.iface_tracklist)

        self.iface_mukke = InterfaceMukke(self.log)
        bus.export("/org/mpris/MediaPlayer2/mukke", self.iface_mukke)

        #await bus.request_name('org.mpris.MediaPlayer2.mukke')
        await bus.request_name("org.mpris.MediaPlayer2.mukke.instance" + str(os.getpid()))

        await bus.wait_for_disconnect()

    def stop(self):
        self.bus.disconnect()
        self.thread.join()

    def state_changed(self, state):
        self.log.debug("update state")
        self.iface_player.set_state(state)


class InterfaceMukke(dbus_next.service.ServiceInterface):

    def __init__(self, log):
        #super().__init__("org.mpris.MediaPlayer2")  # TODO what is expected here? by whom?
        super().__init__("de.elektronenpumpe.mukke")
        self.log = log

class InterfaceTrackList(dbus_next.service.ServiceInterface):

    def __init__(self, log):
        super().__init__('org.mpris.MediaPlayer2.TrackList')
        self.log = log

class InterfacePlaylists(dbus_next.service.ServiceInterface):

    def __init__(self, log):
        super().__init__('org.mpris.MediaPlayer2.Playlists')
        self.log = log


class Interface(dbus_next.service.ServiceInterface):

    def __init__(self, log):
        super().__init__('org.mpris.MediaPlayer2')
        self.log = log

    # annotation org.freedesktop.DBus.Property.EmitsChangedSignal ?!

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def CanQuit(self) -> "b":
        return False  # TODO needed to be false for xfce4 panel volume plugin to show?!

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def CanRaise(self) -> "b":
        return False

    @dbus_next.service.method()
    def Raise(self):
        self.log.debug("Raise")

    @dbus_next.service.method()
    def Quit(self):
        self.log.debug("Quit")

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def HasTrackList(self) -> "b":
        return False

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def Identity(self) -> "s":
        return "Mukke"

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def DesktopEntry(self) -> "s":
        return "mukke"


# https://specifications.freedesktop.org/mpris-spec/latest/Player_Interface.html
class InterfacePlayer(dbus_next.service.ServiceInterface):

    def __init__(self, mpris):
        super().__init__('org.mpris.MediaPlayer2.Player')
        self.mpris = mpris
        self.log = mpris.log
        self.state = None

    def set_state(self, state):
        self.state = state
        self.log.debug("emitting signal %s", self.state)

        status = self._create_status(state)
        self.emit_properties_changed({"PlaybackStatus": status})

        meta = self._create_meta(state)
        self.emit_properties_changed({"Metadata": meta})

    def _create_status(self, state):
        if not state:
            s = "Stopped"
            return s
        if state["state"] == "playing":
            s = "Playing"
        if state["state"] == "paused":
            s = "Paused"
        if state["state"] == "stopped":
            s = "Stopped"
        return s

    def _create_meta(self, state):
        meta = {}
        meta["mpris:trackid"] = dbus_next.Variant("o", "/org/mpris/MediaPlayer2/mukke")
        self.log.debug("metadata state %s", state)
        if state:
            title = state["file"]
        else:
            title = ""
        meta["xesam:title"] = dbus_next.Variant("s", title)
        return meta

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def PlaybackStatus(self) -> "s":
        return self._create_status(self.state)

    @dbus_next.service.method()
    def Play(self):
        self.log.debug("Play")
        self.mpris.operator.enqueue(mukke.events.Play2())

    @dbus_next.service.method()
    def Pause(self):
        self.log.debug("Pause")
        self.mpris.operator.enqueue(mukke.events.Pause())

    @dbus_next.service.method()
    def PlayPause(self):
        self.log.debug("PlayPause")
        if not self.state:
            self.mpris.operator.enqueue(mukke.events.Play2())
            return

        if self.state["state"] == "playing":
            self.mpris.operator.enqueue(mukke.events.Pause())
        else:
            self.mpris.operator.enqueue(mukke.events.Play2())

    @dbus_next.service.method()
    def Stop(self):
        self.log.debug("Stop")
        self.mpris.operator.enqueue(mukke.events.Stop())

    @dbus_next.service.method()
    def Previous(self):
        self.log.debug("Previous")
        self.mpris.operator.enqueue(mukke.events.Prev())

    @dbus_next.service.method()
    def Next(self):
        self.log.debug("Next")
        self.mpris.operator.enqueue(mukke.events.Next())

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def CanPause(self) -> "b":
        return True

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def CanPlay(self) -> "b":
        return True

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def CanGoNext(self) -> "b":
        return True

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def CanGoPrevious(self) -> "b":
        return True

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def Metadata(self) -> "a{sv}":
        return self._create_meta(self.state)

    @dbus_next.service.dbus_property(dbus_next.constants.PropertyAccess.READ)
    def Position(self) -> "x":
        return 0


if __name__ == "__main__":
    print("running mpris test")

    import logging
    log = logging.getLogger()
    hdlr = logging.StreamHandler()
    log.addHandler(hdlr)
    log.setLevel(logging.DEBUG)

    service = MPRIS(None, log)
    service.start()

    import time
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass

    service.stop()



