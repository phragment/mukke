# This file is part of Mukke
# Copyright 2020-2022 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gio

class Monitor:

    def __init__(self, model):
        self.model = model
        self.log = model.log.getChild(type(self).__name__)

        self.monitors = {}

    def add(self, dp):
        self.log.debug("adding file monitor %s", dp)
        gf = Gio.File.new_for_path(dp)
        monitor = gf.monitor_directory(Gio.FileMonitorFlags.WATCH_MOVES)
        monitor.connect("changed", self.on_changed)
        self.monitors[dp] = monitor

    def remove(self, dp):
        self.log.debug("removing file monitor %s", dp)
        fm = self.monitors[dp]
        fm.cancel()
        self.monitors[dp] = None

    def on_changed(self, file_monitor, f, of, event_type):
        fp = f.get_path()
        if of:
            fp_new = of.get_path()
        self.log.debug("file event %s %s", event_type, fp)

        if event_type == Gio.FileMonitorEvent.CREATED:
            self.log.debug("created, add")
            self.model.add_file2(fp)

        if event_type == Gio.FileMonitorEvent.MOVED_IN:
            self.log.debug("moved in, add")
            self.model.add_file2(fp)

        if event_type == Gio.FileMonitorEvent.DELETED:
            self.log.debug("deleted, remove")
            self.model.remove_file(fp)

        if event_type == Gio.FileMonitorEvent.MOVED_OUT:
            self.log.debug("moved out, remove")
            self.model.remove_file(fp)

        if event_type == Gio.FileMonitorEvent.RENAMED:
            self.log.debug("renamed, remove old add new")
            self.model.remove_file(fp)
            self.model.add_file2(fp_new)









