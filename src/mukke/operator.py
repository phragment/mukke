# This file is part of Mukke
# Copyright 2020-2022 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

"""
The Operator processes the Events from the Command Queue.
"""

import collections
import threading

import mukke.model
import mukke.view


class Operator:

    """
    asdf
    """

    def __init__(self, log, settings):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings

        self.queue = Queue()
        self.running = True

        self.view = mukke.view.View(log, settings, self)
        self.model = mukke.model.Model(log, settings, self.view)

        self.log.info("init done")

    def start(self):
        op_thread = threading.Thread(target=self.process, name="OpThread")
        op_thread.start()

        self.model.start()

        # Gtk has to be run from thread which created the objects
        self.view.start()

        self.model.stop()

        op_thread.join()

    def process(self):
        while self.running:
            event = self.queue.get()

            self.cur = event
            event.process(self)
            self.cur = None

    def stop(self):
        self.log.info("stopping")
        self.running = False

    def enqueue(self, cmd):
        self.queue.put(cmd)


class Queue:

    def __init__(self):
        # thread-safe and memory efficient queue providing
        # fast appends and pops from either side
        self.queue = collections.deque()

        # used to let the consumer sleep, NOT for locking
        self.sem = threading.Semaphore(value=0)

    def put(self, cmd):
        self.queue.append(cmd)
        self.sem.release()

    def get(self):
        self.sem.acquire()
        return self.queue.popleft()




